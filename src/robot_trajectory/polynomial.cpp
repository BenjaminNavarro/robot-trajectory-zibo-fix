#include <umrob/polynomial.h>
#include <cmath>

using std::pow;

namespace umrob {

Polynomial::Constraints& Polynomial::constraints() {
    return constraints_;
}

const Polynomial::Constraints& Polynomial::constraints() const {
    return constraints_;
}

// clang-format off
/* Simplified coefficient for xi = 0 and dx = xf-xi
 * a = -(12*yi - 12*yf + 6*dx*dyf + 6*dx*dyi - d2yf*dx^2 + d2yi*dx^2)/(2*dx^5)
 * b = (30*yi - 30*yf + 14*dx*dyf + 16*dx*dyi - 2*d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^4) 
 * c = -(20*yi - 20*yf + 8*dx*dyf + 12*dx*dyi - d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^3) 
 * d = d2yi/2 
 * e = dyi 
 * f = yi
 */
void Polynomial::computeCoefficients() {
// clang-format off
// Simplified coefficient for xi = 0 and dx = xf-xi
// constraints_.xi=0; 
 
 double dx;
 dx =constraints_.xf-constraints_.xi ;
 coefficients_.a = -12.*(constraints_.yi) - 12.*(constraints_.yf ) + 6.*(dx)*(constraints_.dyf) + 6.*(dx)*(constraints_.dyi) - (constraints_.d2yf)*pow(dx,2) + (constraints_.d2yi)*pow(dx,2)/(2.*pow(dx,5)) ;
 coefficients_.b = 30.*constraints_.yi - 30.*constraints_.yf + 14.*dx*constraints_.dyf + 16.*dx*constraints_.dyi - 2.*constraints_.d2yf*pow(dx,2) + 3.*constraints_.d2yi*(pow(dx,2))/(2.*pow(dx,4)) ;
 coefficients_.c = -20*constraints_.yi - 20*constraints_.yf + 8*dx*constraints_.dyf + 12*dx*constraints_.dyi - constraints_.d2yf*pow(dx,2) + 3*constraints_.d2yi*pow(dx,2)/(2.*pow(dx,3)) ;
 coefficients_.d = constraints_.d2yi/2.  ;
 coefficients_.e = constraints_.dyi  ;
 coefficients_.f = constraints_.yi  ;
  
    
    // TODO implement(done)
}
// clang-format on

double Polynomial::evaluate(double x) {
    if (constraints_.xi < constraints_.xf) {

        if (x < constraints_.xi) {
            return constraints_.yi;
        } else if (x > constraints_.xf) {
            return constraints_.yf;
        }
    } else {
        if (x > constraints_.xi) {
            return constraints_.yi;
        } else if (x < constraints_.xf) {
            return constraints_.yf;
        }
    }
    double dx;
    dx=x-constraints_.xi;
    return coefficients_.a * pow(dx, 5) + coefficients_.b * pow(dx, 4) +
           coefficients_.c * pow(dx, 3) + coefficients_.d * pow(dx, 2) +
           coefficients_.e *dx + coefficients_.f;

    // TODO implement (done )
}

//! dy = 5ax^4 + 4bx^3 + 3cx^2 + 2dx + e
double Polynomial::evaluateFirstDerivative(double x) {
    if (constraints_.xi < constraints_.xf) {

        if (x < constraints_.xi) {
            return constraints_.dyi;
        } else if (x > constraints_.xf) {
            return constraints_.dyf;
        }
    } else {
        if (x > constraints_.xi) {
            return constraints_.dyi;
        } else if (x < constraints_.xf) {
            return constraints_.dyf;
        }
    }
    double dx;
    dx=x-constraints_.xi;
    return 5 * coefficients_.a * pow(dx, 4) + 4 * coefficients_.b * pow(dx, 3) +
           3 * coefficients_.c * pow(dx, 2) + 2 * coefficients_.d * dx +
           coefficients_.e;
    // TODO implement
}

//! d2y = 20ax^3 + 12bx^2 + 6cx + 2d
double Polynomial::evaluateSecondDerivative(double x) {
    if (constraints_.xi < constraints_.xf) {

        if (x < constraints_.xi) {
            return constraints_.d2yi;
        } else if (x > constraints_.xf) {
            return constraints_.d2yf;
        }
    } else {
        if (x > constraints_.xi) {
            return constraints_.d2yi;
        } else if (x < constraints_.xf) {
            return constraints_.d2yf;
        }
    }
    double dx;
    dx=x-constraints_.xi;
    return 20 * coefficients_.a * pow(dx, 3) + 12 * coefficients_.b * pow(dx, 2) +
           6 * coefficients_.c * dx + 2 * coefficients_.d;
}

} // namespace umrob