#include <umrob/trajectory_generator.h>
#include <umrob/polynomial.h>
//#include <cstdlib>
#include <fmt/format.h>

#include <numeric>

using namespace std;

namespace umrob {

TrajectoryGenerator::Segment::Segment(const Eigen::Vector6d& max_velocity,
                                      const Eigen::Vector6d& max_acceleration)
    : max_velocity{max_velocity}, max_acceleration{max_acceleration} {
}

TrajectoryGenerator::TrajectoryGenerator(double time_step)
    : time_step_{time_step} {
    reset();
}

void TrajectoryGenerator::startFrom(const Eigen::Affine3d& pose) {
    reset();
    last_waypoint_ = pose;
    target_pose_ = pose;
}

void TrajectoryGenerator::addWaypoint(const Eigen::Affine3d& pose,
                                      const Eigen::Vector6d& max_velocity,
                                      const Eigen::Vector6d& max_acceleration) {

    Segment seg(max_velocity, max_acceleration);
    setSegmentConstraints(last_waypoint_, pose, seg);
    computeSegmentDuration(seg);
    // This
    computeSegmentParameters(seg);
    segments_.push_back(seg);
    last_waypoint_ = pose;
    // Or this
    // addWaypoint(pose, seg.duration);

    // TODO implement ___

    // Créer un segment avec max(vel&acc)
    // Appeler computesegmentduration
    // Mettre la pose dans le segment
    // Appel computesegmentparameter
    // Mettre le segemnt a la fin de la liste des segments
    // Prendre la position à attendre et mettre dans position attente
}

void TrajectoryGenerator::addWaypoint(const Eigen::Affine3d& pose,
                                      double duration) {
    Segment seg;
    setSegmentConstraints(last_waypoint_, pose, seg);
    seg.duration = duration;
    computeSegmentParameters(seg);
    segments_.push_back(seg);
    last_waypoint_ = pose;
    // TODO implement
}

TrajectoryGenerator::State TrajectoryGenerator::update() {
    if (state_ == State::TrajectoryCompleted) {
        return state_;
    } else {
        state_ = State::ReachingWaypoint;
    }

    auto evaluate = [this]() {
        Eigen::Vector6d next_pose_vec;

        // TODO evaluate the polynomial and its derivatives into next_pose_vec,
        // target_velocity_ and target_acceleration_

        for (size_t i = 0; i < 6; i++) {
            auto& poly = currentSegment().polynomials[i];
            next_pose_vec[i] = poly.evaluate(currentSegment().current_time);
            target_velocity_[i] =
                poly.evaluateFirstDerivative(currentSegment().current_time);
            target_acceleration_[i] =
                poly.evaluateSecondDerivative(currentSegment().current_time);
        }
        // Prend les points pour calculer la matrix 
        target_pose_.translation() = next_pose_vec.head<3>();
        target_pose_.linear() =
            Eigen::Quaterniond::fromAngles(next_pose_vec.tail<3>())
                .toRotationMatrix();
    };

    evaluate();

    currentSegment().current_time += time_step_;

    if (currentSegment().current_time > currentSegment().duration) {
        // Mise à  jour de la duréé du segment
        //currentSegment().current_time += currentSegment().duration;
        // currentSegment().duration.evaluate(currentSegment().current_time) ;
        // TODO switch to next segment and re-evaluate if needed
        if(current_segment_idx_==segments_.size()-1){
            state_=State::TrajectoryCompleted;
            evaluate();
        }
        else{
            state_=State::ReachingWaypoint;
            current_segment_idx_++;
        }
    }

    return state_;
}

const Eigen::Affine3d& TrajectoryGenerator::targetPose() const {
    return target_pose_;
}

const Eigen::Vector6d& TrajectoryGenerator::targetVelocity() const {
    return target_velocity_;
}

const Eigen::Vector6d& TrajectoryGenerator::targetAcceleration() const {
    return target_acceleration_;
}

TrajectoryGenerator::State TrajectoryGenerator::state() const {
    return state_;
}

double TrajectoryGenerator::duration() const {
    // TODO implement(faire la somme de toutes les durations )

    long unsigned int i;
    double total_duration = 0;
    for (i = 0; i < segments_.size(); i++) {

        total_duration += segments_[i].duration;
    }
    return total_duration;
}

void TrajectoryGenerator::reset() {
    state_ = State::Idle;
    last_waypoint_.setIdentity();
    segments_.clear();
    current_segment_idx_ = 0;

    target_pose_ = last_waypoint_;
    target_velocity_.setZero();
    target_acceleration_.setZero();
}

void TrajectoryGenerator::setSegmentConstraints(const Eigen::Affine3d& from,
                                                const Eigen::Affine3d& to,
                                                Segment& segment) {
    Eigen::Vector6d from_vec, to_vec;
    from_vec << from.translation(),
        Eigen::Quaterniond(from.linear()).getAngles();
    to_vec << to.translation(), Eigen::Quaterniond(to.linear()).getAngles();

    for (size_t i = 0; i < 6; i++) {
        auto& poly = segment.polynomials[i];
        auto& cstr = poly.constraints();
        cstr.xi = 0;
        cstr.xf = 0;
        cstr.yi = from_vec(i);
        cstr.yf = to_vec(i);
    }
}

//! Tmin_vel = 30|Δy|/16vmax
//! Tmin_acc = sqrt(10sqrt(3)|Δy|/3amax)
void TrajectoryGenerator::computeSegmentDuration(Segment& segment) {

    double Tmin_vel = 0;
    double Tmin_acc = 0;
    for (size_t i = 0; i < 6; i++) {
        auto& cstr = segment.polynomials[i].constraints();
        Tmin_vel = max(Tmin_vel, 30 * abs(cstr.yf - cstr.yi) /
                                     (segment.max_velocity[i]));
        Tmin_acc = max(Tmin_acc, sqrt(10 * sqrt(3) * abs(cstr.yf - cstr.yi)) /
                                     (3 * (segment.max_acceleration[i])));
        
    }

    segment.duration =max(Tmin_vel, Tmin_acc);
    // TODO implement
}

void TrajectoryGenerator::computeSegmentParameters(Segment& segment) {
    // xf comme time
    for (size_t i = 0; i < 6; i++) {
       // segment.polynomials[i].computeCoefficients();
        auto& poly = segment.polynomials[i];
        auto& cstr = poly.constraints();
        cstr.xf=segment.duration;
        poly.computeCoefficients();
        }

    // TODO compute the polynomials coefficients for the current duration
}

TrajectoryGenerator::Segment& TrajectoryGenerator::currentSegment() {
    return segments_.at(current_segment_idx_);
}

} // namespace umrob